/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suwatinee.oxgameui;

import java.io.Serializable;

/**
 *
 * @author suwat
 */
public class Table implements Serializable{

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;

    }

    public void ShowTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    public char getRowCol(int row, int col) {
        return table[row][col];
    }

    public boolean setRowCol(int row, int col) {
        System.out.println(isFinish());
        if (isFinish()) {
            return false;
        }
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            checkWin();
            return true;
        }
        return false;

    }

    public Player getcurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }

    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkX() {
        if (table[1][1] == table[0][0]
                && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            finish = true;
            winner = currentPlayer;

        }
        if (table[0][2] == table[1][1]
                && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            finish = true;
            winner = currentPlayer;

        }
        return;
    }

    public void checkDraw() {
        int sum = 0;
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (table[i][j] != '-') {
                    sum++;
                }
            }
        }
        if (sum == 9 && winner == null) {
            playerO.draw();
            playerX.draw();
            finish = true;
        }
    }

    public void checkWin() {
        checkCol();
        checkRow();
        checkX();
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }
}
